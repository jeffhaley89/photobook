//
//  Photo.swift
//  Photobook
//
//  Created by Jeffrey Haley on 9/3/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import Foundation

let photoCache = NSCache<AnyObject, AnyObject>()

struct Photos: Decodable {
    let photos: [Photo]?
    
    enum CodingKeys: String, CodingKey {
        case photos = "items"
    }
}

struct Photo: Decodable {
    let title: String?
    let media: Image?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case media = "media"
    }
}

struct Image: Decodable {
    let image: String?
    
    enum CodingKeys: String, CodingKey {
        case image = "m"
    }
}
