//
//  FlickrSearchModel.swift
//  Photobook
//
//  Created by Jeffrey Haley on 9/3/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import Foundation


class FlickrSearchModel {
    
    let flickrURL = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
    
    func retrievePhotos(completionHandler: @escaping (Photos?, Error?) -> ()) {
        guard let url = flickrURL else { return }
        DispatchQueue.main.async {
            NetworkManager.get(url, objectType: Photos.self) { (result: NetworkManager.Result<Photos>) in
                switch result {
                case .success(let albums):
                    completionHandler(albums, nil)
                case .failure(let failureError):
                    switch failureError {
                    case .deserializationError(let error), .unknown(let error):
                        completionHandler(nil, error)
                    case .unexpectedResponse:
                        completionHandler(nil, nil)
                    }
                }
            }
        }
    }
    
}
