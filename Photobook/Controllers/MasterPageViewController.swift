//
//  MasterPageViewController.swift
//  Photobook
//
//  Created by Jeffrey Haley on 9/3/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class MasterPageViewController: UIViewController {

    var photoBookModel: [Photo] = [] {
        didSet {
            guard let photoVC = self.createPhotoViewController(index: 0) else { return }
            pageViewController.setViewControllers([photoVC], direction: .forward, animated: false, completion: {done in })
        }
    }
    
    var pageViewController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
    let activityIndicator = UIActivityIndicatorView(style: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        getPhotos()
    }
    
    private func configureView() {
        view.addSubview(activityIndicator)
        activityIndicator.center = self.view.center
        
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        self.addChild(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParent: self)
    }
    
    @objc private func getPhotos() {
        activityIndicator.startAnimating()
        FlickrSearchModel().retrievePhotos { [weak self] (result: Photos?, error: Error?) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                if let photos = result?.photos {
                    self?.photoBookModel = photos
                } else {
                    self?.displaySimpleAlert(message: error?.localizedDescription ?? "Unexpected response")
                }
            }
        }
    }

    private func createPhotoViewController(index: Int) -> PhotoViewController? {
        guard self.photoBookModel.count != 0 && index < self.photoBookModel.count else { return nil }
        let photoViewController = self.storyboard!.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        photoViewController.photoData = self.photoBookModel[index]
        return photoViewController
    }
    
    private func getCurrentPageIndex(vc: UIViewController) -> Int? {
        guard let photoVC = vc as? PhotoViewController else { return nil }
        return photoBookModel.firstIndex(where:{ $0.media?.image == photoVC.photoData?.media?.image })
    }
}

extension MasterPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = getCurrentPageIndex(vc: viewController), index != 0 else { return nil }
        return self.createPhotoViewController(index: index-1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = getCurrentPageIndex(vc: viewController), index != self.photoBookModel.count-1 else { return nil }
        return self.createPhotoViewController(index: index+1)
    }
    
}
