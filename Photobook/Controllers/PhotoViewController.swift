//
//  PhotoViewController.swift
//  Photobook
//
//  Created by Jeffrey Haley on 9/3/19.
//  Copyright © 2019 Jeffrey Haley. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {

    @IBOutlet weak var photoTitle: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    
    var photoData: Photo? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photoTitle?.text = photoData?.title
        setImage(imageURL: photoData?.media?.image)
    }
    
    private func setImage(imageURL: String?) {
        if let imageFromCache = photoCache.object(forKey: imageURL as AnyObject) as? UIImage {
            self.photoImage.image = imageFromCache
        } else {
            guard let url = imageURL else { return }
            loadImage(imageURL: url) { [weak self] loadedImage in
                guard let image = loadedImage else { return }
                self?.photoImage.image = image
                photoCache.setObject(image, forKey: imageURL as AnyObject)
            }
        }
    }
    
    private func loadImage(imageURL: String, completionHandler: @escaping (UIImage?) -> ()) {
        DispatchQueue.global().async {
            if let url = URL(string: imageURL), let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    completionHandler(UIImage(data: data))
                }
            } else {
                completionHandler(nil)
            }
        }
    }

}
